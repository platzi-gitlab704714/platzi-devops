import { TodoEntity } from "../entities/todo.entity";
import { CreateTodoDTO } from '../dtos/todos/create-todo';
import { UpdateTodoDTO } from '../dtos/todos/update-todo';

export abstract class TodoDatasource {
  abstract create(createTodoDTO: CreateTodoDTO): Promise<TodoEntity>
  abstract getAll(): Promise<TodoEntity[]>
  abstract findById(id: number): Promise<TodoEntity>
  abstract updateById(updateTodoDTO: UpdateTodoDTO): Promise<TodoEntity>
  abstract deleteById(id: number): Promise<TodoEntity>
}