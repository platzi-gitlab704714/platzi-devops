import path from 'node:path'
import express, { Router } from 'express'
import compression from 'compression'

interface Options {
  port: number,
  routes: Router
  public_path: string
}

export class Server {
  private readonly app = express()
  private serverListener?: any
  private readonly port: number
  private readonly publicPath: string
  private readonly routes: Router

  constructor(options: Options) {
    const { port, routes, public_path = 'public' } = options
    
    this.port = port
    this.publicPath = public_path
    this.routes = routes
  }

  public get getApp () {
    return this.app
  }

  async start () {
    this.app.use(express.json())
    this.app.use(express.urlencoded({ extended: true }))
    this.app.use(express.static(this.publicPath))
    this.app.use(compression())

    this.app.use(this.routes)

    this.app.get('*', (req, res) => {
      const indexPath = path.join(__dirname + `../../../${this.publicPath}/index.html`)
      res.sendFile(indexPath)
    })

    this.serverListener = this.app.listen(this.port, () => {
      console.log(`Server running on PORT ${this.port}`)
    })
  }

  public close() {
    this.serverListener?.close()
  }
}